<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attribute\AttributeTypes;
use App\Models\AttributeCategory;
use App\Models\AttributeType;
use App\Repositories\Attribute\AttributeTypeRepository;
use App\Repositories\Attribute\AttributeTypeValueRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductAttributeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ProductAttributeRepository();
    }

    public function index()
    {
        $title = 'لیست ویژگی های محصولات';
        $attributes = $this->repository->all(['category','type']);

        return view('admin.attribute.index', compact('title', 'attributes'));

    }

    public function create()
    {
        $attributeTypeRepo=new AttributeTypeRepository();
        $title = 'ایجاد ویژگی جدید';
        $allAttributeCategory = AttributeCategory::all();
        $types = AttributeTypes::getTypes();
        $contentType=$attributeTypeRepo->all();
        return view('admin.attribute.create', compact('title', 'allAttributeCategory', 'types','contentType'));
    }

    public function store(Request $request)
    {
        $newAttribute = $this->repository->create([
            'attribute_name' => $request->input('attribute_name'),
            'attribute_category_id' => $request->input('attribute_category'),
            'attribute_type_id' => $request->input('attribute_type_id'),
            'attribute_type' => $request->input('attribute_type'),
            'is_searchable' => $request->has('is_searchable'),
            'is_filterable' => $request->has('attribute_type'),
        ]);
        if ($newAttribute) {

            return redirect()->back()->with('success', true);
        }

    }

    public function category_create()
    {
        $title = 'ایجاد دسته بندی ویژگی ها';
        return view('admin.attribute_category.create', compact('title'));
    }

    public function category_index()
    {
        $title = 'لیست دسته بندی ویژگی ها';
        $allAttributeCategory = AttributeCategory::all();
        return view('admin.attribute_category.index', compact('title', 'allAttributeCategory'));
    }

    public function category_store(Request $request)
    {
        $newAttributeCategory = $this->repository->saveCategory([

            'attribute_category_name' => $request->input('attribute_category_name')
        ]);
        if ($newAttributeCategory) {
            return redirect()->back()->with('success', true);
        }
    }

    public function type_index()
    {
        $title = 'لیست نوع ویژگی ها';
        $allAttributeType = AttributeType::all();
        return view('admin.attribute_type.index', compact('title', 'allAttributeType'));

    }

    public function type_create()
    {
        $title = 'ایجاد نوع ویژگی ها';
        return view('admin.attribute_type.create', compact('title'));


    }

    public function type_store(Request $request)
    {
        $newAttributeType = $this->repository->saveType([
            'attribute_type_name' => $request->input('attribute_type_name')

        ]);
        if ($newAttributeType) {
            return redirect()->back()->with('success', true);
        }

    }


    public function getCategoryAttribute(Request $request, $pid)
    {
        $category = new CategoryRepository();
        $attributes = $category->getAttributesByCategoryId($pid);
//        $attributeHtml = view('admin.attribute.render.item', compact('attributes'));
        $attributeHtml = view('admin.attribute.render.item',compact('attributes'))->render();
        return $attributeHtml;


    }


    public function getTypeOptions($type_id)
    {
        $title = 'ایجاد مقدار نوع';
        return view('admin.attribute_type.options', compact('title', 'type_id'));

    }

    public function saveTypeOptions(Request $request, $type_id)
    {
        $attributeTypeValueRepo = new AttributeTypeValueRepository();
        $result = $attributeTypeValueRepo->create([
            'attribute_type_id' => $type_id,
            'attribute_type_value' => $request->input('attribute_type_value')
        ]);
        if ($result) {
            return redirect()->back()->with('success', true);
        }

    }

}
