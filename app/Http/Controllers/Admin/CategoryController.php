<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductAttributeRepository;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends BaseController
{
    private $attribute_repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CategoryRepository();
        $this->attribute_repository = new ProductAttributeRepository();
    }

    public function index()
    {
        $title = 'لیست  دسته بندی ها';
        $categories = $this->repository->all(['parent']);
        return view('admin.category.index', compact('title', 'categories'));
    }

    public function edit()
    {
        $title = 'ایجاد دسته بندی محصولات';
        $categories = $this->repository->all();
        $allAttributes = $this->attribute_repository->all();
        return view('admin.category.create', compact('title', 'categories', 'allAttributes'));
    }


    public function store(CategoryRequest $request)
    {
        $newCategory = $this->repository->create([
            'category_title' => $request->input('category_title'),
            'category_slug' => $request->input('category_slug'),
            'category_parent_id' => $request->input('category_parent_id'),
        ]);
        if ($newCategory and is_a($newCategory, Category::class)) {
            $newCategory->attributes()->sync($request->input('product_category_attributes'));
            return redirect()->back()->with(['success' => true]);
        }

    }
}
