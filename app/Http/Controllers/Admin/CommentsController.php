<?php

namespace App\Http\Controllers\Admin;


use App\Models\Product;
use App\Repositories\Comment\CommentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentsController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new CommentRepository();

    }

    public function index(Request $request)
    {
        $comments = $this->repository->all();
        if ($request->has('pid') && intval($request->input('pid')) > 0) {
            $comments=$this->repository->commentByModel($request->input('pid'));
        }

        $title = 'لیست دیدگاه ها';
        return view('admin.comment.index', compact('title', 'comments'));
    }
}
