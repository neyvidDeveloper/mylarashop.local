<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\SendInvoice;
use App\Models\Order\orderstatus;
use App\Repositories\Order\OrderRepository;
use App\Services\Order\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends BaseController
{
    public $orderService;
    public function __construct()
    {
        parent::__construct();
        $this->repository = new OrderRepository();
        $this->orderService=new OrderService();

    }


    public function index()
    {
        $title = 'لیست سفارش ها';
        $orders = $this->repository->all(['user']);
        return view('admin.order.index', compact('title', 'orders'));
    }

    public function create()
    {

        $order = $this->orderService->create([
            'user_id' => 1,
            'total_amount' => 1500000,
            'order_discount' => 0,
            'order_items' => [
                ['product_id' => 1, 'amount' => 400000, 'discount' => 0],
                ['product_id' => 2, 'amount' => 600000, 'discount' => 0],
                ['product_id' => 3, 'amount' => 500000, 'discount' => 0]
            ]
        ]);
//        SendInvoice::dispatch($order)
//            ->delay(now()->addMinutes(20))
//            ->onQueue('Emails')
//            ->onConnection('sqs');
    }

    public function approve(Request $request,int $order_id)
    {
        $orderItem=$this->repository->find($order_id);
        $orderItem->updateStatus(orderstatus::PAID);
        $this->orderService->updateStatus($order_id,$orderItem->order_status,orderstatus::PAID);

    }
}
