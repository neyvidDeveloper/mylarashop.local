<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ProductRepository();
    }

    public function index()
    {
        $title = 'لیست محصولات';
        $products = $this->repository->all(['comments']);
        $categories = (new CategoryRepository())->all();
        return view('admin.product.index', compact('title', 'products', 'categories'));
    }

    public function edit()
    {
        $categories = (new CategoryRepository())->all()->groupBy('category_parent_id');
        $title = 'اضافه کردن محصول جدید';
        return view('admin.product.create', compact('title', 'categories'));
    }

    public function store(ProductRequest $request)
    {

        $newProduct = $this->repository->create([
            'product_code' => 0,
            'product_category_id' => $request->input('product_category_id'),
            'product_title' => $request->input('product_title'),
            'product_slug' => '',
            'product_price' => $request->input('product_price'),
            'product_stock' => $request->input('product_stock'),
            'product_discount' => $request->input('product_discount'),
            'product_type' => $request->input('product_type'),
            'product_coupon_count' => $request->input('product_coupon_count'),
            'product_description' => $request->input('product_description'),
            'product_status' => $request->input('product_status'),
            'product_visible' => $request->exists('product_visible') ? 1 : 0
        ]);

        if ($newProduct && is_a($newProduct, Product::class)) {
            $allAttributes = $request->input('product_attributes');
            $formatedAttributes = [];
            foreach ($allAttributes as $attribute_id => $attribute_value) {
                $formatedAttributes[$attribute_id] = ['attribute_value'=>$attribute_value];
            }

            $newProduct->attributes()->sync($formatedAttributes);
        }

    }


}
