<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Requests\ResgisterRequest;
use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthConroller extends BaseController
{
    public function showLogin()
    {
        $title = 'ورود به سایت';
        return view('auth.login', compact('title'));
    }


    public function doLogin(Request $request)
    {
        if (Auth::attempt(['email' => $request->input('user_email'), 'password' => $request->input('user_password')])) {
            return redirect()->route('home');
        }
    }

    public function showRegister()
    {
        $title = 'ثبت نام در سایت';
        return view('auth.register', compact('title'));
    }

    public function doRegister(ResgisterRequest $request)
    {
        UserService::create($request->except('_token'));

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
