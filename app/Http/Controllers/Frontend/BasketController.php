<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Admin\BaseController;
use App\Repositories\Product\ProductRepository;
use App\Services\Basket\BasketService;
use App\Services\Order\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasketController extends BaseController
{
    public function __construct()
    {
        $this->repository = new ProductRepository();
    }

    public function add(Request $request, $product_id)
    {

        BasketService::add($product_id);



    }

    public function show()
    {
        $title = 'لیست سبد خرید';
        $productItemInBasket = session()->get('basket.items');
        return view('frontend.basket.show', compact('title', 'productItemInBasket'));

    }

    public function checkout()
    {
        $productItemInBasket = session()->get('basket.items');
        $total_amount = 0;
        $orderData = [
            'user_id' => 1,
            'total_amount' => 0,
            'order_discount' => 0,
            'order_items' => []
        ];
        foreach ($productItemInBasket as $key => $item) {
            foreach ($item as $itemKey => $itemValue) {
                $orderData['order_items'][] = [
                    'product_id' => $itemValue['product']->product_id,
                    'amount' => $itemValue['product']->product_price,
                    'discount' => 0
                ];

                $total_amount += $itemValue['product']->product_price * $itemValue['count'];

            }
        }
        $orderData['total_amount'] = $total_amount;
        $orderService = new OrderService();
        $orderService->create($orderData);

    }
}
