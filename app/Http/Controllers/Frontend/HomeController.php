<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Admin\BaseController;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends BaseController
{
    public function __construct()
    {
        $this->repository=new ProductRepository();
    }

    public function index()
    {
        $title='';
        $products=$this->repository->all();
        return view('frontend.home',compact('title','products'));
    }
}
