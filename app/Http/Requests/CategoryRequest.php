<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_title' => 'required',
            'category_slug' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'category_title.required' => 'لطفا فیلد عنوان دسته بندی را تکمیل نمایید',
            'category_slug.required' => 'لطفا نامک را تکمیل نمایید'
        ];
    }
}
