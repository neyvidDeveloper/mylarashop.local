<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_title' => 'required',
            'product_price' => 'required|integer',
            'product_stock' => 'required|integer',
            'product_discount' => 'integer',
            'product_type' => 'required|integer',
            'product_coupon_count' => 'integer',
            'product_status' => 'required|integer',
            'product_description' => 'required',
            'product_visible' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'product_title.required' => 'لطفا عنوان محصول را تکمیل نمایید',
            'product_price.required' => 'لطفا قیمت محصول را تکمیل نمایید',
            'product_price.integer' => 'لطفا قیمت محصول را به عدد واردنمایید',
            'product_stock.required' => 'لطفا تعداد محصول را تکمیل نمایید',
            'product_stock.integer' => 'لطفا تعداد محصول رابه عدد واردنمایید',
            'product_discount.integer' => 'لطفا تخفیف محصول را تکمیل نمایید',
            'product_type.required' => 'لطفا نوع محصول را تکمیل نمایید',
            'product_coupon_count.integer' => 'لطفا تعداد کوپن محصول را تکمیل نمایید',
            'product_status.required' => 'لطفا وضعیت محصول را انتخاب نمایید',
            'product_description.required' => 'لطفا توضیحات محصول را تکمیل نمایید',
            'product_visible.required' => 'لطفا وضعیت نمایش محصول را انتخاب نمایید نمایید'
        ];
    }
}
