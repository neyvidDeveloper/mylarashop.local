<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResgisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_fullname'=>'required',
            'user_email'=>'required|email',
            'user_password'=>'required',
        ];
    }
    public function messages()
    {
       return[
           'user_fullname.required'=>'نام و نام خانوادگی را وارد نمایید',
           'user_email.required'=>'ایمیل خود را وارد نمایید',
           'user_email.email'=>'ایمیل خود را بصورت صحیح وارد نمایید',
           'user_password.required'=>'رمز عبورخود را وارد نمایید',
       ];
    }
}
