<?php

namespace App\Models;

use App\Traits\HasAttachment;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasAttachment;
    public $timestamps=false;
    protected $guarded=['attachment_id'];
    protected $primaryKey='attachment_id';
}
