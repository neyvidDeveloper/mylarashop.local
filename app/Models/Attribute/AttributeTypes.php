<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/17/18
 * Time: 6:10 PM
 */

namespace App\Models\Attribute;


class AttributeTypes
{
    const TEXT = 1;
    const DROP_DOWN = 2;
    const RADIO = 3;
    const CHECKBOX = 4;
    const TEXT_AREA = 4;

    public static function getTypes()
    {
        return [
            self::TEXT => 'متنی',
            self::DROP_DOWN => 'کشویی',
            self::RADIO => 'تک گزینه ای',
            self::CHECKBOX => 'چندگزینه ای',
            self::TEXT_AREA => 'متن بزرگ',
        ];
    }
    public static function getBladeType()
    {
        return [
            self::TEXT => 'text',
            self::DROP_DOWN => 'dropdown',
            self::RADIO => 'radio',
            self::CHECKBOX => 'checkbox',
            self::TEXT_AREA => 'textarea'
        ];
    }

    public static function getBladeByType(int $type)
    {
        $blade=self::getBladeType();
        return $blade[$type];
    }
}