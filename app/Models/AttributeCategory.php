<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeCategory extends Model
{
    protected $primaryKey = 'attribute_category_id';
    protected $guarded = ['attribute_category_id'];

    public function all_attributes()
    {
        return $this->hasMany(Attribute::class, 'attribute_category_id');
    }
}
