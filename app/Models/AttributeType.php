<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeType extends Model
{
    protected $primaryKey='attribute_type_id';
    protected $guarded=['attribute_type_id'];

    public function all_attributes()
    {
        return $this->hasMany(Attribute::class,'attribute_type_id');
    }

    public function values()
    {
        return $this->hasMany(AttributeTypeValue::class,'attribute_type_id');
    }

}
