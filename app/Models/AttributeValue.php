<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $primaryKey = 'attribute_value_id';
    protected $guarded = ['attribute_value_id'];
}
