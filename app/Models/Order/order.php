<?php

namespace App\Models\Order;


use App\Models\User;
use App\Presenters\Contract\Presentable;
use App\Presenters\OrderPresenter;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use Presentable;
    protected $presenter = OrderPresenter::class;
    protected $primaryKey = 'order_id';
    protected $guarded = ['order_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'order_user_id');
    }

    public function order_items()
    {
        return $this->hasMany(orderItem::class, 'order_item_order_id');

    }

    public function orderLogs()
    {
        return $this->hasMany(OrderLog::class, 'order_log_order_id');

    }

    public function updateStatus(int $status)
    {
        if(!in_array($status,array_keys(orderstatus::getOrderStatuses())))
        {
            return false;
        }
        $this->order_status=$status;
        $this->save();

    }
}
