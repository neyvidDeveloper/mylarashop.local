<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class orderItem extends Model
{
    protected $guarded = ['order_item_id'];
    protected $primaryKey = 'order_item_id';

    public function order()
    {
        $this->belongsTo(order::class, 'order_item_order_id');
    }
}
