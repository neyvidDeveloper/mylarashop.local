<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/20/18
 * Time: 9:18 PM
 */

namespace App\Models\Order;


class orderstatus
{
    const UNPAID = 0;
    const PAID = 1;
    const CANCELED = 2;
    const REFOUND = 3;
    const DELIVERED = 4;
    const SENT = 5;
    const SENT_READY = 6;

    public static function getOrderStatuses()
    {
        return [
            self::UNPAID => 'پرداخت نشده',
            self::PAID => 'پرداخت شده',
            self::CANCELED => 'لغو شده',
            self::REFOUND => 'مرجوع شده',
            self::DELIVERED => 'تحویل داده شده',
            self::SENT => 'ارسال شده',
            self::SENT_READY => 'آماده ارسال',
        ];

    }

    public static function getOrderStatus(int $status)
    {
        return self::getOrderStatuses()[$status];
    }

}