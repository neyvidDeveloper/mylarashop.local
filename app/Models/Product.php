<?php

namespace App\Models;


use App\Presenters\Contract\Presentable;
use App\Presenters\ProductPresenter;
use App\Traits\HasAttribute;
use App\Traits\HasComment;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Presentable, HasComment,HasAttribute;

    protected $primaryKey = 'product_id';
    protected $guarded = ['product_id'];
    protected $presenter = ProductPresenter::class;

//
//    public function getProductPriceAttribute()
//    {
//       return $this->attributes['product_price'].'هزار تومان';
//    }

    public function setProductSlugAttribute()
    {
        $this->attributes['product_slug'] = preg_replace('/\s+/', '-', $this->attributes['product_title']);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'product_category_id');
    }




}
