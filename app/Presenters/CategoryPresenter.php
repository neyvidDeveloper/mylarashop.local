<?php


namespace App\Presenters;


use App\Presenters\Contract\Presenter;

class CategoryPresenter extends Presenter
{
    public function category_title_parent()
    {
        return isset($this->entity->parent)? $this->entity->parent->category_title : 0;
    }

}