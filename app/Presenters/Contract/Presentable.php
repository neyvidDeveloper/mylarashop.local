<?php


namespace App\Presenters\Contract;


trait Presentable
{
    public function present()
    {
        if (!$this->presenter || !class_exists($this->presenter)) {
            throw new \Exception('This Class Not Found');
        }

        return new $this->presenter($this);

    }


}