<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/27/18
 * Time: 10:49 AM
 */

namespace App\Presenters;


use App\Models\Order\orderstatus;
use App\Presenters\Contract\Presenter;

class OrderPresenter extends Presenter
{
    public function showStatusOfOrder()
    {
        $order_status=$this->order_status;
        return orderstatus::getOrderStatus($order_status);

    }

    public function oprations()
    {
        $order=$this;
        return view('admin.order.oprations',compact('order'));

    }
    function isOrderApprovable(){
        return in_array($this->order_status,[orderstatus::UNPAID,orderstatus::CANCELED]);
    }

}