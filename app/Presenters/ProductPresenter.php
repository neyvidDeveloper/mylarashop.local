<?php


namespace App\Presenters;


use App\Presenters\Contract\Presenter;

class ProductPresenter extends Presenter
{

    public function product_price_in_hezar_toman()
    {
        return number_format($this->product_price / 1000) . 'هزار تومان';
    }

    public function show_category_of_each_product()
    {
        $categories = $this->categories;
        if (isset($categories) && count($categories) > 0)  {
            $categoryName = '';
            foreach ($this->categories as $category) {
                $categoryName .= $category->category_title . ' و ';
            }
            echo rtrim($categoryName, ' و ');

        } else {
            echo '--';
        }


    }

    public function show_image_of_status()
    {
        if ($this->product_status == 1) {
            echo '<img src="/images/unlock.png" alt="">';

        } else {
            echo '<img src="/images/locked.png" alt="">';
        }

    }


}