<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/18/18
 * Time: 7:54 PM
 */

namespace App\Repositories\Attribute;


use App\Models\AttributeType;
use App\Repositories\Contract\BaseRepository;


class AttributeTypeRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = AttributeType::class;
    }

}