<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/18/18
 * Time: 7:19 PM
 */

namespace App\Repositories\Attribute;


use App\Models\AttributeTypeValue;
use App\Repositories\Contract\BaseRepository;

class AttributeTypeValueRepository extends BaseRepository

{
    public function __construct()
    {
        $this->model=AttributeTypeValue::class;
    }

}