<?php


namespace App\Repositories\Comment;


use App\Models\Comment;
use App\Models\Product;
use App\Repositories\Contract\BaseRepository;


class CommentRepository extends BaseRepository
{

    public function __construct()
    {
        parent::__construct();
        $this->model = Comment::class;
    }

    public function commentByModel(int $model_id, string $model_type = Product::class)
    {
        $model_object = $model_type::find($model_id);
        return $model_object->comments;
    }
}