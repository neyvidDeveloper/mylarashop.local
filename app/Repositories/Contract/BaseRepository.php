<?php


namespace App\Repositories\Contract;


use App\Models\Product;

abstract class BaseRepository
{
    protected $model;

    public function __construct()
    {
        
    }
    public function all(array $eagerLoadItem=[])
    {
        if(!empty($eagerLoadItem)){
            return $this->model::with($eagerLoadItem)->get();
        }
        return $this->model::all();
    }

    public function find($id)
    {
        return $this->model::find($id);
    }
    public function create(array $data)
    {
        return $this->model::create($data);
    }

}