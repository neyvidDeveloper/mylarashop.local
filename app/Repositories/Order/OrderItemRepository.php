<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/20/18
 * Time: 8:40 PM
 */

namespace App\Repositories\Order;


use App\Models\Order\orderItem;
use App\Repositories\Contract\BaseRepository;

class OrderItemRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model=orderItem::class;
    }


}