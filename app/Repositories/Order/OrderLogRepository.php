<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/24/18
 * Time: 12:05 PM
 */

namespace App\Repositories\Order;


use App\Models\Order\OrderLog;
use App\Repositories\Contract\BaseRepository;

class OrderLogRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model=OrderLog::class;
    }

}