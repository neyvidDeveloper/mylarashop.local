<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/20/18
 * Time: 8:39 PM
 */

namespace App\Repositories\Order;


use App\Models\Order\order;
use App\Repositories\Contract\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model=order::class;

    }

}