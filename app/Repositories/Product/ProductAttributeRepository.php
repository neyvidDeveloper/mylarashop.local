<?php


namespace App\Repositories\Product;


use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\AttributeType;
use App\Repositories\Contract\BaseRepository;

class ProductAttributeRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Attribute::class;
    }

    public function saveCategory(array $data)
    {
        return AttributeCategory::create($data);

    }

    public function saveType(array $data)
    {
        return AttributeType::create($data);
    }

}