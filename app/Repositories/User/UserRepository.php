<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 3/6/18
 * Time: 2:38 PM
 */

namespace App\Repositories\User;


use App\Models\User;
use App\Repositories\Contract\BaseRepository;


class UserRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model=User::class;
    }
}