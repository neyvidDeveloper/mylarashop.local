<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 3/4/18
 * Time: 10:15 AM
 */

namespace App\Services\Basket;


use App\Repositories\Product\ProductRepository;

class BasketService
{

    public static function add(int $product_id)
    {
        $productRepository = new ProductRepository();
        $productItems = $productRepository->find($product_id);
        if ($productItems) {
            $items = session('basket.items');
            if (is_null($items)) {
                $items = [];
            }
            if (isset($items[$product_id])) {
                $items[$product_id]['count'] += 1;
            } else {
                $items[$product_id] = [
                    'product' => $productItems,
                    'count' => 1,
                ];
            }
            session(['basket.items' => $items]);
        }

    }

    public static function items()
    {
        return session('basket.items');
    }
}