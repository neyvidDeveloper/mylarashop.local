<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/20/18
 * Time: 8:53 PM
 */

namespace App\Services\Discount;


class DiscountService
{
    public static function calculateByPercent(int $amount, int $percent)
    {
        if($percent==0){
            return $amount;
        }
        return (1 - ($percent / 100)) * $amount;
    }

    public static function calculateByValue(int $amount, int $value)
    {
        return $amount-$value;
    }

}