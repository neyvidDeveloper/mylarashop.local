<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/20/18
 * Time: 8:24 PM
 */

namespace App\Services\Order;


use App\Events\OrderCreated;
use App\Jobs\SendInvoice;
use App\Models\Order\OrderLog;
use App\Models\Order\orderstatus;
use App\Repositories\Order\OrderItemRepository;
use App\Repositories\Order\OrderLogRepository;
use App\Repositories\Order\OrderRepository;
use App\Services\Discount\DiscountService;

class OrderService
{
    private $orderRepository;
    private $orderItemRepository;
    private $orderLogRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->orderItemRepository = new OrderItemRepository();
        $this->orderLogRepository = new OrderLogRepository();
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {

        $newOrder = $this->orderRepository->create([
            'order_user_id' => $data['user_id'],
            'order_total_amount' => $data['total_amount'],
            'order_discount' => $data['order_discount'],
            'order_payable_amount' => DiscountService::calculateByPercent($data['total_amount'], $data['order_discount']),
            'order_status' => orderstatus::UNPAID
        ]);

        $orderItems = [];

        foreach ($data['order_items'] as $order_item) {
            $orderItems[] = [
                'order_item_order_id' => $newOrder->order_id,
                'order_item_product_id' => $order_item['product_id'],
                'order_item_amount' => $order_item['amount'],
                'order_item_product_discount' => $order_item['discount'],
                'order_item_payable_amount' => DiscountService::calculateByPercent($order_item['amount'], $order_item['discount'])
            ];

        }
        $createdOrderItems = $newOrder->order_items()->createMany($orderItems);
        if ($newOrder) {
            event(new OrderCreated($newOrder));
            return [
                'success' => true,
                'order' => $newOrder,
                'orderItem' => $createdOrderItems
            ];
        }
    }

    public function updateStatus(int $orderId, int $currentstatus, int $nextStatus, string $description = null)
    {
        $currentUser = 1;
        $resutl = $this->orderLogRepository->create([
            'order_log_order_id' => $orderId,
            'order_log_current_status' => $currentstatus,
            'order_log_next_status' => $nextStatus,
            'order_log_agent' => $currentUser,
            'order_log_description' => $description
        ]);
        return $resutl instanceof OrderLog;
//        if($resutl instanceof OrderLog){
//            return true;
//        }
//        return false;


    }
}