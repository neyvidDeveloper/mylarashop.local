<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/24/18
 * Time: 12:52 PM
 */

namespace App\Services\Payment\Contract;


abstract class OnlineGateway
{

    abstract public function PaymentRequest(array $params);
    abstract public function VerifyPayment();
}