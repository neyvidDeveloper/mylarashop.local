<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/24/18
 * Time: 10:13 AM
 */

namespace App\Services\Payment\Contract;


use App\Repositories\Order\OrderRepository;

abstract class PaymentMethod
{
    protected $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    abstract public function pay(int $order_id);


}