<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 2/24/18
 * Time: 10:17 AM
 */

namespace App\Services\Payment\PaymentMethods;


use App\Models\Order\orderstatus;
use App\Services\Order\OrderService;
use App\Services\Payment\Contract\PaymentMethod;

class Account extends PaymentMethod

{

    public function pay(int $order_id)
    {
        $orderItems = $this->orderRepository->find($order_id);
        $orderOwner = $orderItems->user;
        if ($orderOwner->balance >= $orderItems->order_total_amount) {
            $orderItems->status = orderstatus::PAID;
            $orderItems->save();
            $orderOwner->decrement('balance', $orderItems->order_total_amont);
            $orderService = new OrderService();
            $updateResult = $orderService->updateStatus($order_id, orderstatus::UNPAID, orderstatus::PAID);

        }
    }
}