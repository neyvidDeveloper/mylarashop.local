<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 3/6/18
 * Time: 2:37 PM
 */

namespace App\Services\User;


use App\Events\UserRegistered;
use App\Models\User;
use App\Repositories\User\UserRepository;

class UserService
{
    public static function create(array $data)
    {
        $userRepository = new UserRepository();
        $newUser = $userRepository->create([
            'name' => $data['user_fullname'],
            'email' => $data['user_email'],
            'password' => $data['user_password']
        ]);
        if ($newUser && $newUser instanceof User) {
//            UserRegistered::dispatch($newUser);
            event(new UserRegistered($newUser));
        }
        return $newUser;
    }

}