<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 3/11/18
 * Time: 3:40 PM
 */

namespace App\Traits;


use App\Models\Attachment;

trait HasAttachment
{
    public function attachments()
    {
        return $this->morphToMany(Attachment::class,
            'attachables'
        );
    }
}