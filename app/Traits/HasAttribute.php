<?php


namespace App\Traits;


use App\Models\Attribute;

trait HasAttribute
{

    public function attributes()
    {
        return $this->morphToMany(Attribute::class,
            'attributable',
            'attributables',
        'attributable_id',
        'attribute_id'
            );
    }
}