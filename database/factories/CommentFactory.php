<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Comment::class, function (Faker $faker) {
    return [
        'comment_user_id' => array_random([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'commentable_id' => array_random([1, 2,]),
        'commentable_type' => \App\Models\Product::class,
        'comment_parent_id' => 0,
        'comment_body' => $faker->text,
        'comment_source' => 0,
        'comment_ip' => $faker->ipv4
    ];
});
