<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_user_id');
            $table->integer('order_payment_method');
            $table->integer('order_total_amount');
            $table->integer('order_discount');
            $table->integer('order_payable_amount');
            $table->text('order_discount_description');
            $table->text('order_description');
            $table->integer('order_shipping_method');
            $table->integer('order_shipping_amount');
            $table->integer('order_packaging_amount');
            $table->integer('order_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
