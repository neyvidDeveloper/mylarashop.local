var gulp = require('gulp');
var rtlcss = require('gulp-rtlcss');

gulp.task('default', function () {
    return gulp.src('./public/css/bootstrap.css')
        .pipe(rtlcss())
        .pipe(gulp.dest('./public/css/rtl'));
});