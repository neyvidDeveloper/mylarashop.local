@extends('layouts.admin')
@section('content')

    <div class="forms">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>{{$title}}</h4>
            </div>
            <div class="form-body">
                @include('admin.attribute.errors')
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>ویژگی جدید با موفقیت ایجاد گردید‍</p>
                    </div>
                @endif
                <form method="post" action="{{route('admin.attribute.store')}}">
                    {{csrf_field()}}
                    <div class="form-group"><label for="exampleInputEmail1">نام ویژگی</label>
                        <input type="text" class="form-control" name="attribute_name" id="attribute_name">
                    </div>

                    <div class="form-group"><label for="exampleInputPassword1">دسته ویژگی</label>
                        <select name="attribute_category" class="form-control" id="attribute_category">
                            @foreach($allAttributeCategory as $attributeCategory)

                                <option value="{{$attributeCategory->attribute_category_id}}">{{$attributeCategory->attribute_category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">نوع ویژگی</label>
                        <select name="attribute_type_id" class="form-control" id="attribute_type_id">
                            @if($contentType && count($contentType)>0)
                                @foreach($contentType as $contentTypeItem)
                                    <option value="{{ $contentTypeItem->attribute_type_id }}">{{ $contentTypeItem->attribute_type_name }}</option>

                                @endforeach
                            @endif


                        </select>
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">نوع نمایش</label>
                        <select name="attribute_type" class="form-control" id="attribute_type">
                            @if($types && count($types)>0)
                                @foreach($types as $type_id=>$type)
                                    <option value="{{ $type_id }}">{{ $type }}</option>

                                @endforeach
                            @endif


                        </select>
                    </div>


                    <div class="checkbox col-sm-12"><label>
                            <input type="checkbox" name="is_searchable">فابلیت جستجو
                        </label></div>


                    <div class="checkbox col-sm-12"><label>
                            <input type="checkbox" name=is_filterable">فابلیت فیلتر
                        </label></div>
                    <button type="submit" class="btn btn-default">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>

    </div>

@endsection