@extends('layouts.admin')
@section('content')
    <div class="tables">
        <div class="table-responsive bs-example widget-shadow">
            <h4>{{$title}}</h4>
            <table class="table table-bordered">
                @include('admin.attribute.column')
                <tbody>
                <?php $count=1;?>
                @foreach($attributes as $attribute)
                @include('admin.attribute.item')
                <?php $count++;?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection
