
    <label for="attribute-{{$item->attribute_id}}">{{$item->attribute_name}}</label>
    <select
            name="product_attributes[{{$item->attribute_id}}]"
            class="form-control"
            id="attribute-{{$item->attribute_id}}">
        @foreach($item->type->values as $valueItem)
            <option value="">{{ $valueItem->attribute_type_value }}</option>
        @endforeach
    </select>


