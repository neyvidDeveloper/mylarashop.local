@extends('layouts.admin')
@section('content')

    <div class="forms">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>{{$title}}</h4>
            </div>
            <div class="form-body">
                @include('admin.attribute_category.errors')
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>دسته بندی ویژگی با موفقیت ایجاد گردید‍</p>
                    </div>
                @endif
                <form method="post" action="{{route('admin.attribute.category.store')}}">
                    {{csrf_field()}}
                    <div class="form-group"><label for="attribute_category_name">نام دسته بندی</label>
                        <input type="text" class="form-control" name="attribute_category_name" id="attribute_category_name">
                    </div>


                    <button type="submit" class="btn btn-default">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>

    </div>

@endsection