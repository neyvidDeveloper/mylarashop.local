@extends('layouts.admin')
@section('content')
    <div class="tables">
        <div class="table-responsive bs-example widget-shadow">
            <h4>{{$title}}</h4>
            <table class="table table-bordered">
                @include('admin.attribute_category.column')
                <tbody>
                <?php $count=1;?>
                @foreach($allAttributeCategory as $attributeCategory)
                    @include('admin.attribute_category.item')
                <?php  $count++;?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection
