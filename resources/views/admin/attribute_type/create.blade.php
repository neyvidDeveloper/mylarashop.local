@extends('layouts.admin')
@section('content')

    <div class="forms">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>{{$title}}</h4>
            </div>
            <div class="form-body">
                @include('admin.attribute_category.errors')
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>نوع ویژگی با موفقیت ایجاد گردید‍</p>
                    </div>
                @endif
                <form method="post" action="{{route('admin.attribute.type.store')}}">
                    {{csrf_field()}}
                    <div class="form-group"><label for="attribute_type_name">نام نوع ویژگی</label>
                        <input type="text" class="form-control" name="attribute_type_name" id="attribute_type_name">
                    </div>


                    <button type="submit" class="btn btn-default">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>

    </div>

@endsection