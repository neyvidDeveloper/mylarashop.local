@extends('layouts.admin')
@section('content')
    <div class="tables">
        <div class="table-responsive bs-example widget-shadow">
            <h4>{{$title}}</h4>
            <table class="table table-bordered">
                @include('admin.attribute_type.column')
                <tbody>
                <?php $count=1;?>
                @foreach($allAttributeType as $attributeType)
                    @include('admin.attribute_type.item')
                    <?php $count++;?>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@endsection
