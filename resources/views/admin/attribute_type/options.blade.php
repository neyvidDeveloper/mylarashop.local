@extends('layouts.admin')
@section('content')

    <div class="forms">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>{{$title}}</h4>
            </div>
            <div class="form-body">
                @include('admin.attribute_category.errors')
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>نوع ویژگی با موفقیت ایجاد گردید‍</p>
                    </div>
                @endif
                <form method="post" action="{{route('admin.attribute.type.options.save',$type_id)}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="attribute_type_value">مقدار نوع ویژگی</label>
                        <input type="text" class="form-control" name="attribute_type_value" id="attribute_type_value">
                    </div>


                    <button type="submit" class="btn btn-default">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>

    </div>

@endsection