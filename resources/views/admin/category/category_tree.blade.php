@if($collection && count($collection)>0)
    <ol style="list-style-type:none">
        @foreach($collection as $category)
            @include('admin.category.category_tree_item')
        @endforeach
    </ol>
@endif