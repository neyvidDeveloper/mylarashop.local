<li>
    <input type="radio" name="product_category_id"
           value="{{$category->category_id}}"
           @change="getProductAttribute({{$category->category_id}})">
    {{$category->category_title}}
</li>
@if(isset($categories[$category->category_id]) && count($categories[$category->category_id])>0)
    @include('admin.category.category_tree',['collection'=>$categories[$category->category_id]])
@endif