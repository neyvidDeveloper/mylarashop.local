@extends('layouts.admin')
@section('content')
    <div class="forms">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>{{$title}}</h4>
            </div>
            <div class="form-body">
                @include('admin.category.errors')
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>دسته بندی جدید با موفقیت ایجاد گردید‍</p>
                    </div>
                @endif

                <form method="post" action="{{route('admin.category.store')}}">
                    {{csrf_field()}}
                    <div class="form-group"><label for="exampleInputEmail1">عنوان دسته بندی</label>
                        <input type="text" class="form-control" name="category_title" id="category_title">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">نامک دسته بندی</label>
                        <input type="text" class="form-control" name="category_slug" id="category_slug">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">والد دسته</label>
                        <select name="category_parent_id" class="form-control" id="category_parent_id">
                            @if($categories && count($categories)>0)
                                <option value="0">بدون والد</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->category_id}}">{{$category->category_title}}</option>
                                @endforeach
                            @else
                                <option value="0">بدون والد</option>
                            @endif


                        </select>
                    </div>
                    <div class="checkbox col-sm-12">
                        @foreach($allAttributes as $attribute)
                            <label>
                                <input type="checkbox" value="{{$attribute->attribute_id}}"
                                       name="product_category_attributes[]">
                                {{$attribute->attribute_name}}
                            </label>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-default">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>

    </div>

@endsection