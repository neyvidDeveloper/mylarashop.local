@extends('layouts.admin')
@section('content')
    <div class="tables">
        <div class="table-responsive bs-example widget-shadow">
            <h4>{{$title}}</h4>
            <table class="table table-bordered">
                @include('admin.category.column')
                <tbody>
                @if($categories && count($categories)>0)
                    <?php $count=1; ?>
                    @foreach($categories as $category)
                        @include('admin.category.item')
                        <?php $count++; ?>
                    @endforeach
                @else
                    @include('admin.category.no-item')
                @endif

                </tbody>
            </table>
        </div>
    </div>

@endsection
