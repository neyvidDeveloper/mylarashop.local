@extends('layouts.admin')


@section('content')
    <div class="tables">
        <div class="table-responsive bs-example widget-shadow">
            <h4>{{$title}}</h4>
            <table class="table table-bordered">
                @include('admin.comment.column')
                <tbody>
                @if($comments && count($comments)>0)
                    <?php $count = 1; ?>
                    @foreach($comments as $comment)
                        @include('admin.comment.item')
                        <?php $count++; ?>
                    @endforeach
                @endif


                </tbody>
            </table>
        </div>
    </div>

@endsection