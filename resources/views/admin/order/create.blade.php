@extends('layouts.admin')
@section('content')

    <div class="forms">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>{{$title}}</h4>
            </div>
            <div class="form-body">
                @include('admin.product.errors')

                <form method="post" action="{{route('admin.products.store')}}">
                    {{csrf_field()}}
                    <div class="form-group"><label for="exampleInputEmail1">عنوان محصول</label>
                        <input type="text" class="form-control" name="product_title" id="product_title">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">قیمت محصول</label>
                        <input type="text" class="form-control" name="product_price" id="product_price">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">موجودی محصول</label>
                        <input type="text" class="form-control" name="product_stock" id="product_stock">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">تخفیف به درصد</label>
                        <input type="text" class="form-control" name="product_discount" id="product_discount">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">نوع محصول</label>
                        <select name="product_type" class="form-control" id="product_type">
                            <option value="1">فیزیکی</option>
                            <option value="2">مجازی</option>
                        </select>
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">تعداد کوپن</label>
                        <input type="text" class="form-control" name="product_coupon_count" id="product_coupon_count">
                    </div>

                    <div class="checkbox col-sm-12">
                        <label> دسته بندی محصولات </label>
                        @include('admin.category.category_tree',['collection'=>$categories[0]])
                    </div>
                    <div class="checkbox col-sm-12">
                        <label> ویژگی ها</label>
                        <div v-if="attributeHtml" v-html="attributeHtml">

                        </div>
                    </div>


                    <div class="form-group"><label for="exampleInputPassword1">توضیحات محصول</label>
                        <textarea name="product_description" class="form-control" id="product_description" cols="30"
                                  rows="10"></textarea>
                    </div>
                    {{--<div class="form-group">--}}
                    {{--<label for="radio" class="col-sm-2 control-label">وضعیت محصول</label>--}}
                    {{--<div class="col-sm-8">--}}
                    {{--<div class="radio-inline"><label><input type="radio" checked="" name="product_status">عادی</label></div>--}}
                    {{--<div class="radio-inline"><label><input type="radio"  name="product_status">پیش فروش</label></div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label for="radio" class="col-sm-2 control-label">وضعیت محصول</label>
                        <div class="col-sm-12">
                            <div class="radio block"><label><input type="radio" name="product_status"
                                                                   value="1">عادی</label></div>
                            <div class="radio block"><label><input type="radio" name="product_status" value="2"> پیش
                                    فروش</label></div>

                        </div>
                    </div>

                    <div class="checkbox col-sm-12"><label> <input type="checkbox" name="product_visible">نمایش محصول
                        </label></div>
                    <button type="submit" class="btn btn-default">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>

    </div>

@endsection