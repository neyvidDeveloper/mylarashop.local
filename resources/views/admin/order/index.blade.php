@extends('layouts.admin')
@section('content')
    <div class="tables">
        <div class="table-responsive bs-example widget-shadow">
            <h4>{{$title}}</h4>
            <table class="table table-bordered">
                @include('admin.order.column')
                <tbody>
                @if($orders && count($orders)>0)
                    <?php $count = 1; ?>
                    @foreach($orders as $order)
                        @include('admin.order.item')

                        <?php $count++; ?>
                    @endforeach
                @else
                    @include('admin.order.no-item')
                @endif

                </tbody>
            </table>
        </div>
    </div>

@endsection
