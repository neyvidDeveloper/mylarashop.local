@extends('layouts.admin')
@section('content')

    <div class="forms">
        <div class="form-grids row widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>{{$title}}</h4>
            </div>
            <div class="form-body">
                @include('admin.product.errors')

                <form method="post" action="{{route('admin.products.store')}}">
                    {{csrf_field()}}
                    <div class="form-group"><label for="exampleInputEmail1">عنوان محصول</label>
                        <input type="text" class="form-control" name="product_title" id="product_title">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">قیمت محصول</label>
                        <input type="text" class="form-control" name="product_price" id="product_price">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">موجودی محصول</label>
                        <input type="text" class="form-control" name="product_stock" id="product_stock">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">تخفیف به درصد</label>
                        <input type="text" class="form-control" name="product_discount" id="product_discount">
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">نوع محصول</label>
                        <select name="product_type" class="form-control" id="product_type">
                            <option value="1">فیزیکی</option>
                            <option value="2">مجازی</option>
                        </select>
                    </div>
                    <div class="form-group"><label for="exampleInputPassword1">تعداد کوپن</label>

                        <input type="text" class="form-control" name="product_coupon_count" id="product_coupon_count">
                    </div>
                    <div class="checkbox col-sm-12">
                        <label> دسته بندی محصولات </label>
                        @include('admin.category.category_tree',['collection'=>$categories[0]])
                    </div>
                    <div class="checkbox col-sm-12">
                        <label> ویژگی ها</label>
                        <div v-if="attributeHtml" v-html="attributeHtml">

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="radio" >وضعیت محصول</label>
                        <div class="col-sm-12">
                            <div class="radio block">

                                <input type="radio" name="product_status" id="desc_product" value="1">عادی
                            </div>
                            <div class="radio block">
                                <input type="radio" name="product_status" id="desc_product" value="2">پیش فروش
                            </div>

                        </div>
                    </div>
                    <div class="form-group"><label for="product_image">تصویر محصول</label>

                        <input type="file" class="form-control" name="product_image" id="product_image">


                    </div>
                    <div class="checkbox col-sm-12">
                        <input type="checkbox" name="product_visible">نمایش محصول
                    </div>
                    <button type="submit" class="btn btn-default">ذخیره اطلاعات</button>
                </form>
            </div>
        </div>
    </div>

@endsection