@extends('layouts.admin')
@section('content')
    <div class="tables">
        <div class="table-responsive bs-example widget-shadow">
            <h4>{{$title}}</h4>
            <table class="table table-bordered">
                @include('admin.product.column')
                <tbody>
                @if($products && count($products)>0)
                    <?php $count = 1; ?>
                    @foreach($products as $product)
                        @include('admin.product.item')

                        <?php $count++; ?>
                    @endforeach
                @else
                    @include('admin.product.no-item')
                @endif

                </tbody>
            </table>
        </div>
    </div>

@endsection
