@extends('layouts.frontend')
@include('partials.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ورود به سایت
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="{{route('login.login')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="user_email" class="col-sm-2 control-label">ایمیل</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="user_email" name="user_email"
                                           placeholder="ایمیل خود را وارد نمایید">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user_password" class="col-sm-2 control-label">کلمه عبور</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="user_password" id="user_password"
                                           placeholder="رمز عبور را واردنمایید">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> مرا به خاطر بسپار
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">ورود</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection