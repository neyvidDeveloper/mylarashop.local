@extends('layouts.frontend')
@include('partials.navbar')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        ورود به سایت
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="{{route('login.register')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="user_full_name"  class="col-sm-2 control-label">نام کامل</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="user_full_name" name="user_fullname" placeholder="نام و نام خانوادگی را وارد نمایید">
                                </div>
                            </div>  <div class="form-group">
                                <label for="user_email"  class="col-sm-2 control-label">ایمیل</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="ایمیل خود را وارد نمایید">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user_password" class="col-sm-2 control-label">کلمه عبور</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="user_password" id="user_password"
                                           placeholder="رمز عبور را واردنمایید">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">ثبت نام</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection