@extends('layouts.frontend')
@section('content')
    @include('partials.navbar')
    <div class="heder">

    </div>
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">{{$title}}</div>

                <!-- Table -->
                <table class="table">
                    @include('frontend.basket.column')
                    @if($productItemInBasket && count($productItemInBasket)>0)
                        @foreach($productItemInBasket as $product_id=>$item)
                            @include('frontend.basket.item')
                        @endforeach
                    @endif
                </table>

            </div>
            <div class="basket details">

                <a class="btn btn-primary" href="/basket/checkout">تکمیل سفارش</a>
            </div>

        </div>
    </div>
@endsection