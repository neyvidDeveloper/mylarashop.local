@extends('layouts.frontend')
@section('content')
    @include('partials.navbar')
    <div class="heder">

    </div>
    <div class="container">
        <div class="row">
           @each('frontend.products.items',$products,'product')
        </div>
    </div>
@endsection