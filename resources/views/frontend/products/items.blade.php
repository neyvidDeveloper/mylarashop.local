<div class="col-xs-12 col-md-3 col-lg-4 ">
    <div class="product" id="product_{{$product->product_id}}">
        <div class="product_thumbnail">
            <img src="http://via.placeholder.com/250x350">
        </div>
        <div class="product_title">
            {{$product->product_title}}
        </div>
        <div class="product_price">
            {{$product->present()->product_price_in_hezar_toman()}}
        </div>
        <div class="add_to_basket">
            <a class="btn-success btn btn-block" href="/basket/add/{{$product->product_id}}">اضافه کردن به سبد خرید</a>
        </div>
    </div>
</div>


