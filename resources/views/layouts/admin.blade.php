@include('partials.header')
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page" id="app">

                    @yield('content')



        <!-- for amcharts js -->
            <script src="/js/amcharts.js"></script>
            <script src="/js/serial.js"></script>
            <script src="/js/export.min.js"></script>
            <link rel="stylesheet" href="/css/export.css" type="text/css" media="all"/>
            <script src="/js/light.js"></script>
            <!-- for amcharts js -->

            <script src="/js/index1.js"></script>
        </div>
    </div>
@include('partials.footer')