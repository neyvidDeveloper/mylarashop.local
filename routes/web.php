<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::group(['namespace'=>'Frontend'],function (){
    Route::get('/','HomeController@index')->name('home');
    Route::get('/basket/add/{product_id}','BasketController@add')->name('basket.add');
    Route::get('/basket','BasketController@show')->name('basket.show');
    Route::get('/basket/checkout','BasketController@checkout')->name('basket.checkout');
    Route::get('auth/login','AuthConroller@showLogin')->name('login.showlogin');
    Route::post('auth/login','AuthConroller@doLogin')->name('login.login');
    Route::get('auth/register','AuthConroller@showRegister')->name('login.showregister');
    Route::post('auth/register','AuthConroller@doRegister')->name('login.register');
    Route::get('logOut','AuthConroller@logOut')->name('logOut');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin','middleware'=>'admin'], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('products', 'ProductController@index')->name('admin.products');
    Route::get('products/create', 'ProductController@edit')->name('admin.products.create');
    Route::post('products/create', 'ProductController@store')->name('admin.products.store');
    Route::get('products/category/create', 'CategoryController@edit')->name('admin.category.create');
    Route::post('products/category/create', 'CategoryController@store')->name('admin.category.store');
    Route::get('products/category', 'CategoryController@index')->name('admin.category.index');
    Route::get('products/comment/{pid?}', 'CommentsController@index')->name('admin.comments');

    Route::get('products/attribute', 'AttributeController@index')->name('admin.attribute.index');
    Route::get('products/attribute/create', 'AttributeController@create')->name('admin.attribute.create');
    Route::post('products/attribute/create', 'AttributeController@store')->name('admin.attribute.store');

    Route::get('products/attribute/categories', 'AttributeController@category_index')->name('admin.attribute.category.index');
    Route::get('products/attribute/categories/create', 'AttributeController@category_create')->name('admin.attribute.category.create');
    Route::post('products/attribute/categories/create', 'AttributeController@category_store')->name('admin.attribute.category.store');

    Route::get('products/attribute/types', 'AttributeController@type_index')->name('admin.attribute.type.index');
    Route::get('products/attribute/types/create', 'AttributeController@type_create')->name('admin.attribute.type.create');
    Route::post('products/attribute/types/create', 'AttributeController@type_store')->name('admin.attribute.type.store');
    Route::post('products/attribute/{pid}', 'AttributeController@getCategoryAttribute')->name('admin.attribute.ajax');
    Route::get('products/attribute/options/{type_id}', 'AttributeController@getTypeOptions')->name('admin.attribute.type.options');
    Route::post('products/attribute/options/{type_id}', 'AttributeController@saveTypeOptions')->name('admin.attribute.type.options.save');

//orders
    Route::get('orders', 'OrderController@index')->name('admin.orders');
    Route::get('orders/create', 'OrderController@create')->name('admin.orders.create');
    Route::get('orders/approve/{order_id}', 'OrderController@approve')->name('admin.orders.approve');

});

